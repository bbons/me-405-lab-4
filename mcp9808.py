'''
@file mcp9808.py
@brief This file contains the driver class for the MCP9808 Temperature Sensor.
@details The full code for this file can be found at https://bitbucket.org/bbons/me-405-lab-4/src/master/mcp9808.py .

@author Ben Bons
@author Michael Yiu

@date Last Updated: February 10, 2021
'''
import pyb

class mcp9808:
    '''
    @brief This class is a driver for the MCP9808 sensor
    @details This class allows for reading temperatures off of the MCP9808 sensor. The class has
    the ability to check if the sensor is connected properly, and temperatures in celsius and 
    fahrenheit. 
    '''
    
    def __init__(self, bus, address):
        '''
        @brief Constructor for the mcp9808 driver object
        @details The constructor will take an initialized I2C bus and the sensor address for the sensor.
        
        @param bus The I2C bus initialized according to pyb.I2C . The bus must be set to master mode.
        @param address The numerical I2C address for the MCP9808 sensor.
        '''
        ## The I2C bus configured to master mode
        self.bus  = bus
        
        ## The numerical address of the sensor
        self.address = address
    
    def check(self):
        '''
        @brief Checks if the MCP9808 sensor is connected properly
        @details The MCP9808 stores the manufacturer ID 0x54 in its 0x06 register. This method
        will read that register and throw a NameError if the correct value is not read off of the machine
        '''
        try:
            data = bytearray(2)
            self.bus.mem_read(data, self.address, 0x06, addr_size = 8)
            #print(data)
            if not(int.from_bytes(data, 'big') == 0x54):
                raise NameError('Identifier not correct')
        except:
            print('Error in connection to MCP9808 sensor')
            raise
        else:
            print('Connected to MCP9808 sensor')
    def celsius(self):
        '''
        @brief Reads the temperature in Celsius off of the temperature sensor
        @details This function reads the 0x05 register on the MCP9808 and implements the algorithm in the datasheet to return the temperature in
        Celsius. Temperature is returned with a number to a 1/16 degree Celsius, but the sensor is accurate to +/- 0.25 degrees Celsius typically,
        +/- 0.5 maximum.
        
        @return The temperature in Celsius
        '''
        data = bytearray(2)
        self.bus.mem_read(data, self.address, 0x05, addr_size = 8)
        return self.convert_bytes(data)
    
    def fahrenheit(self):
        '''
        @brief Returns the temperature in Fahrenheit.
        @details This function calls celsius() and then converts the temperature to Fahrenheit.
        
        @return The temperature in Fahrenheit.
        '''
        temp = self.celsius()
        return 9/5 * temp + 32
    ## \cond
    def convert_bytes(self, num_arr):
        # Reference algorithm on page 25 of the MCP9808 datasheet
        global temperature
        temperature = 0
        num_arr[0] = num_arr[0] & 0x1F # Remove flag variables
        if (num_arr[0] & 0x10 == 0x10): #Negative number
            
            num_arr[0] = num_arr[0] & 0x0F # Remove sign
            temperature = 256 - (num_arr[0]*16 + num_arr[1]/16)
        else:
            
            temperature = num_arr[0] * 16 + num_arr[1]/16
            
        return temperature
    ## \endcond
## \cond
if __name__ == '__main__':
    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    addr = i2c.scan()
    sensor = mcp9808(i2c, addr[0])
    sensor.check()
    while True:
        pyb.delay(1000)
        print('Temperature: {:.2f} degrees Celsius'.format(sensor.celsius()))
## \endcond         
         
     